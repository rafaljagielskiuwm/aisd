﻿#include "pch.h"
#include <iostream>
#include <Windows.h>
#include <ctime>
using namespace std;
const int n = 25;
const int t = 0;
int tab[n];
void generate(int tab[]) {
	for (int i = 0; i < n; i++)
	{
		tab[i] = rand() % 51;
	}
}
void swap(int &a, int &b){
	int tmp = a;
	a = b;
	b = tmp;
}
void drawColumn(int n) {
	if (n == 0) return;
	else {
		cout << "- ";
		drawColumn(n - 1);
	}
}
void drawTab(int tab[], int &check, int &spr) {
	for (int i = 0; i < n; i++)
	{
		if (&check == &tab[i]) cout << " change if this: ";
		else if (&spr == &tab[i]) cout << "is smaller than: ";
		else cout << "                 ";
		cout << tab[i];
		drawColumn(tab[i]);
		cout << endl;
	}
}
void bubbleStandard(int tab[]) {
	for (int i = 1; i < n; i++)
		for (int j = n - 1; j >= i; j--){
			Sleep(t);
			system("cls");
			cout << "Standard Bubble Sort\n";
			drawTab(tab, tab[j], tab[j-1]);
			if (tab[j] < tab[j - 1]) {
				swap(tab[j], tab[j-1]);
			}
		}
}
void betterBubble(int tab[]){
	bool change = true;
	bool check;
	int top = 1;
	int bottom=n-1;
	for (int i = 1; i < n; i++) {
		check = change;
		if (change) {
			for (int j = bottom; j >= top; j--) {
				Sleep(t);
				system("cls");
				cout << "Better Bubble Sort\n";
				drawTab(tab, tab[j], tab[j - 1]);
				if (tab[j] < tab[j - 1]) {
					swap(tab[j], tab[j - 1]);
					change = false;
				}
			}
			top++;
		}
		else {
			for (int j = top; j <= bottom; j++) {
				Sleep(t);
				system("cls");
				cout << "Better Bubble Sort\n";
				drawTab(tab, tab[j], tab[j - 1]);
				if (tab[j] < tab[j - 1]) {
					swap(tab[j], tab[j - 1]);
					change = true;
				}
			}
			bottom--;
		}
		if (check == change) break;
	}
}
int main()
{
	srand(time(NULL));
	generate(tab);
	betterBubble(tab);
	return 0;
	
}


