#include "pch.h"
#include <iostream>
#include <Windows.h>
using namespace std;
void space(int n) {
	if (n < 1) return;
	else {
		cout << " ";
		space(n - 1);
	}
}
int pow(int n) {
	if (n < 1) return 1;
	else return pow(n - 1) * 2;
}
int countStart(int n) {
	if (n == 0) return 0;
	else return n * 2 - 1;
}
class Tree {
public:
	struct Node {
		int data;
		int y;
		int x;
		Node * ancestor;
		Node * left;
		Node * right;
		Node() : ancestor(NULL), left(NULL), right(NULL) {}
	};
	static int lowest;
	Node * root = NULL;
	void add(int liczba) {
		int level = 0;
		int xcord = 1;
		Node * append = new Node;
		if (root == NULL) {
			root = append;
			root->data = liczba;
			root->x = 1;
			root->y = 0;
		}
		else {
			level++;
			Node * temp = new Node;
			temp = root;
			while (!append->ancestor) {
				if ((liczba < temp->data) && (!temp->left)) {
					append->ancestor = temp;
					append->data = liczba;
					temp->left = append;
					xcord = (xcord * 2) - 1;
					break;
				}
				else if (liczba < temp->data) {
					temp = temp->left;
					level++;
					xcord = (xcord * 2) - 1;
				}
				if ((liczba >= temp->data) && (!temp->right)) {
					append->ancestor = temp;
					append->data = liczba;
					temp->right = append;
					xcord *= 2;
					break;
				}
				else if (liczba >= temp->data) {
					temp = temp->right;
					level++;
					xcord *= 2;
				}
			}
			append->y = level;
			append->x = xcord;
		}
		if (level > lowest) lowest = level;
	}
	void gotoxy(int x, int y)
	{
		COORD c;
		c.X = x - 1;
		c.Y = y - 1;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	}
	int countX(Node * temp) {
		int level = lowest - temp->y;
		int diff = pow(level) * 10;
		int start = (pow(level) - 1) * 5;
		return 5 + start + (temp->x - 1)*diff;
	}
	void draw_tree(Node * temp) {
		int y = 2;
		int x = 5;
		if (!temp)
			return;
		else {
			gotoxy(countX(temp), y*(temp->y + 1)); if (temp) cout << temp->data; else cout << "*";
			draw_tree(temp->left);
			draw_tree(temp->right);
		}
	}
};
int Tree::lowest = 0;

int main()
{
	Tree test;

	test.add(10);
	test.add(5);
	test.add(3);
	test.add(7);
	test.add(15);
	test.add(13);
	test.add(17);
	test.add(1);
	test.add(4);
	test.add(6);
	test.add(8);
	test.add(12);
	test.add(14);
	test.add(16);
	test.add(18);
	test.draw_tree(test.root);
}


