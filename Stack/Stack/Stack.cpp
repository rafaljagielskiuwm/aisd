#include "pch.h"
#include <iostream>
#include <conio.h>
using namespace std;
int maxStackElem = 0;
struct listEl {
	listEl * next;
	int data;
};

class stack {
private:
	listEl * S;
public:
	stack();
	~stack();
	bool empty();
	void push(int v);
	int pop();
	void show();
	listEl*top();
};
stack::stack() {
	S = NULL;
}
stack::~stack() {
	while (S)pop();
}
bool stack::empty() {
	return !S;
}
listEl*stack::top() {
	return S;
}
void stack::push(int v) {
	static int count = 0;
	if (count < maxStackElem) {
		count++;
		listEl* e = new listEl;
		e->data = v;
		e->next = S;
		S = e;
	}
	else cout << "Maksymalna liczba lementow na stosie!\n";
}
int stack::pop() {
	int liczba;
	if (S) {
		listEl*e = S;
		S = S->next;
		liczba = e->data;
		delete e;
		return liczba;
	}
	return 0;
}
void stack::show() {
	listEl*temp = S;
	if (!temp) {
		cout << "Stos jest pusty!\n";
	}
	while (temp) {
		cout << temp->data << endl;
		temp = temp->next;
	}
}
void menu() {
	cout << "Wybierz co chcesz zrobic: \n";
	cout << "0.koniec\n";
	cout << "1.push<liczba>\n";
	cout << "2.pop\n";
	cout << "3.wypisz\n";
}
int main()
{
	stack S;
	char choice = 'a';
	int liczba;
	cout << "Podaj maksymalna ilosc elemntow na stosie: ";
	cin >> maxStackElem;
	while (choice != '0') {
		menu();
		choice = _getch();
		switch (choice) {
		case '0': {
			cout << "Zakonczono program\n";
		}
		case '1': {
			cout << "Podaj liczbe ktora chcesz umiescic na stosie: ";
			cin >> liczba;
			S.push(liczba);
			break;
		}
		case'2': {
			cout << "Usunieto wierzcholek stosu, byla nim liczba: " << S.pop() << endl;
			break;
		}
		case'3': {
			S.show();
			break;
		}
		default: {
			cout << "Wybrano zla opcje\n";
			break;
		}
		}
		_getch();
		system("pause");
		system("cls");
	}
}