﻿#include "pch.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
int calcSize(string wejscie) {
	int result = 0;
	int x;
	fstream plik_we;
	plik_we.open(wejscie, std::ios::in | std::ios::out);
	if (plik_we.good() == true)
	{
		while (plik_we >> x) result++;
		plik_we.close();
	}
	return result;
}
void dataToTab(string wejscie, int tab[], int rozmiar) {
	fstream plik_we;
	plik_we.open(wejscie, std::ios::in | std::ios::out);
	if (plik_we.good() == true)
	{
		for (int i = 0; i < rozmiar; i++) plik_we >> tab[i];
		plik_we.close();
	}
	else cout << "Nie udalo sie otworzyc pliku" << endl;
}
void writeToFile(string wyjscie, int tab[], int rozmiar) {
	fstream plik_wy(wyjscie, ios::out);
	if (plik_wy.good())
	{
		for (int i = 0; i < rozmiar; i++)
		{
			plik_wy << tab[i] << " ";
			plik_wy.flush();
		}
		plik_wy.close();
	}
	else cout << "Blad" << endl;
}
void print(int tab[], int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		cout << tab[i] << ", ";
	}
	cout << endl;
}
void scalaj(int tab1[], int rozmiar1, int tab2[], int rozmiar2, int tab3[]) {
	for (int i = 0, j = 0, k = 0; k < rozmiar1+rozmiar2; k++) {
		if (i == rozmiar1) {
			tab3[k] = tab2[j++];
			continue;
		}
		if (j == rozmiar2) {
			tab3[k] = tab1[i++];
			continue;
		}
		if (tab1[i] < tab2[j])
			tab3[k] = tab1[i++];
		else
			tab3[k] = tab2[j++];
	}
}
void zaznaczPowt(int liczba, int *tab, int rozmiar, bool *spr) {
	int licz = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		if (liczba == tab[i]) {
			licz++;
			if ((licz >= 2) && (!spr[i])) spr[i] = true;
			else if (!spr[i]) spr[i] = false;
		}
	}
}
int main()
{
	string plik1 = "plik1.txt";
	string plik2 = "plik2.txt";
	string wynik = "wynik.txt";
	int rozmiar1, rozmiar2, rozmiar_wynikowy=0;
	int * tab1;
	int * tab2;
	int * tab3;
	int * wynikowa;
	bool * spr;
	rozmiar1 = calcSize(plik1);
	rozmiar2 = calcSize(plik2);
	tab1 = new int[rozmiar1];
	tab2 = new int[rozmiar2];
	tab3 = new int[rozmiar1 + rozmiar2];
	spr=new bool[rozmiar1 + rozmiar2];
	for (int i = 0; i < rozmiar1+rozmiar2; i++)
	{
		spr[i] = false;
	}
	dataToTab(plik1, tab1, rozmiar1);
	dataToTab(plik2, tab2, rozmiar2);
	cout << "Liczby z pierwszego pliku: ";
	print(tab1, rozmiar1);
	cout << "Liczby z drugiego pliku: ";
	print(tab2, rozmiar2);
	scalaj(tab1, rozmiar1, tab2, rozmiar2, tab3);
	cout << "Scalone liczby z dwoch plikow z powtorkami: ";
	print(tab3, rozmiar1+rozmiar2);
	for (int i = 0; i < rozmiar1+rozmiar2; i++)
	{
		zaznaczPowt(tab3[i], tab3, rozmiar1 + rozmiar2, spr);
	}
	for (int i = 0; i < rozmiar1+rozmiar2; i++)
	{
		if (!spr[i]) rozmiar_wynikowy++;
	}
	wynikowa = new int[rozmiar_wynikowy];
	for (int i = 0, j=0; i < rozmiar1+rozmiar2; i++)
	{
		if (!spr[i]) wynikowa[j++] = tab3[i];
	}
	cout << "Wynikowa: ";
	print(wynikowa, rozmiar_wynikowy);
	writeToFile(wynik, wynikowa, rozmiar_wynikowy);
	delete[] tab1;
	delete[] tab2;
	delete[] tab3;
	delete[] spr;
	delete[] wynikowa;
}