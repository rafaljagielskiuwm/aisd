#include "pch.h"
#include <iostream>
#include <conio.h>
using namespace std;
const int MAXINT = -2147483647;
static int maxQueueElements;
static int queueElementsCounting = 0;
struct dataStruct {
	int data;
	dataStruct* next; 
};
class queue {
private:
	dataStruct * tail;
public:
	queue();
	~queue();
	bool empty();
	int front();
	void push(int liczba);
	int pop();
	void show();
};
queue::queue() {
	tail = 0;
}
queue::~queue() {
	while (tail) pop();
}
bool queue::empty() {
	return tail;
}
int queue::front() {
	if (tail)return tail->next->data;
	else return -MAXINT;
}
void queue::push(int liczba) {
	dataStruct*newData = new dataStruct;
	newData->data = liczba;
	if (maxQueueElements > queueElementsCounting) {
		if (tail) {
			queueElementsCounting++;
			newData->next = tail->next;
			tail->next = newData;
		}
		else {
			queueElementsCounting++;
			newData->next = newData;
			tail = newData;
		}
	}
	else cout << "Kolejka jest zapelniona\n";
}
int queue::pop(){
	int liczba;
	if (tail) {
		queueElementsCounting--;
		dataStruct *newData = tail->next;
		if (newData->next != newData)tail->next = newData -> next;
		else tail = 0;
		liczba = newData->data;
		delete newData;
		return liczba;
	}
	return MAXINT;
}
void queue::show() {
	if (!tail)cout << "Koleika jest pusta\n";
	else {
		dataStruct*temp = tail;
		for (int i = 0; i < queueElementsCounting; i++)
		{
			cout << temp->data<<endl;
			temp = temp->next;
		}
	}
}
void menu() {
	cout << "Wybierz co chcesz zrobic\n";
	cout << "0.Koniec\n";
	cout << "1.Put<liczba>\n";
	cout << "2.Get\n";
	cout << "3.Wypisz\n";
}
int main()
{
	queue Q;
	char choice = 'a';
	int liczba;
	cout << "Podaj maksymalna dlugosc kolejki: ";
	cin >> maxQueueElements;
	while (choice != '0') {
		menu();
		choice = _getch();
		switch (choice) {
		case '0': {
			break;
		}
		case '1': {
			cout << "Podaj liczbe ktora chesz wpisac do kolejki: ";
			cin >> liczba;
			Q.push(liczba);
			break;
		}
		case '2': {
			cout << "Wyjeto element z koleiki, jego wartosc to: " << Q.pop() << endl;
			break;
		}
		case '3': {
			Q.show();
			break;
		}
		default: {
			cout << "Wybrano zla opcje\n";
			break;
		}
		}
		_getch();
		system("pause");
		system("cls");
	}

}