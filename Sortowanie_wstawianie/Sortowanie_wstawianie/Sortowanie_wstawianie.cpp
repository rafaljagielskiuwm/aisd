#include "pch.h"
#include <iostream>
#include <string>
#include <fstream>
#include <time.h>
#include <windows.h>
using namespace std;
void swap(int &a, int &b) {
	int temp = a;
	a = b;
	b = temp;
}
void InsertSort(int tab[], int size) {
	for (int i = 0; i <size; i++)
	{
		int j = i;
		int temp = tab[j];
		while ((j > 0) && (tab[j - 1]>temp)) {
			tab[j] = tab[j - 1];
			j--;
		}
		tab[j] = temp;
	}
}
//----------------------------------UTWORZENIE PLIKU TEST.TXT--------------------------
void makeTestFile() {
	int amount = (rand() % 10000) + 10000;
	fstream plik_wy("test.txt", ios::out);
	if (plik_wy.good())
	{
		for (int i = 0; i < amount; i++)
		{
			plik_wy << rand()%100 << " ";
			plik_wy.flush();
		}
		plik_wy.close();
	}
	else cout << "Blad" << endl;
}
//----------------------------------OBLICZANIE ILOSCI LICZB W PLIKU--------------------
int calcSize(string wejscie) {
	int result=0;
	int x;
	fstream plik_we;
	plik_we.open(wejscie, std::ios::in | std::ios::out);
	if (plik_we.good() == true)
	{
		while (plik_we >> x) result++;
		plik_we.close();
	}
	return result;
}
//----------------------------------PRZEPISANIE LICZB Z PLIKU DO TABLICY---------------
void dataToTab(string wejscie, int tab[], int rozmiar) {
	fstream plik_we;
	plik_we.open(wejscie, std::ios::in | std::ios::out);
	if (plik_we.good() == true)
	{
		for (int i = 0; i < rozmiar; i++) plik_we >> tab[i];
		plik_we.close();
	}
	else cout << "Nie udalo sie otworzyc pliku" << endl;
}
//----------------------------------SUMA LICZB W TABLICY-------------------------------
int sum(int tab[], int rozmiar) {
	int sum = 0;
	for (int i = 0; i < rozmiar; i++) sum += tab[i];
	return sum;
}
//----------------------------------ZAPIS DO PLIKU-------------------------------------
void writeToFile(string wyjscie, int tab[], int rozmiar) {
	fstream plik_wy(wyjscie, ios::out);
	if (plik_wy.good())
	{
		for (int i = 0; i < rozmiar; i++)
		{
			plik_wy << tab[i] << " ";
			plik_wy.flush();
		}
		plik_wy.close();
	}
	else cout << "Blad" << endl;
}
int main()
{
//----------------------------------UTWORZENIE PLIKU TEST.TXT--------------------------
	srand(time(NULL));
	makeTestFile();
//----------------------------------ZMIENNE--------------------------------------------
	int *tab;
	int rozmiar;
	int i = 0;
	double time;
	int sum_before;
	int sum_after;
	clock_t start, stop;
	string enter;
	string exit;
//----------------------------------PROGRAM WLASCIWY-----------------------------------
	cout << "Podaj nazwe pliku wejsciowego: ";
	cin >> enter;
	rozmiar = calcSize(enter);
	tab = new int[rozmiar];
	dataToTab(enter, tab, rozmiar);
	sum_before = sum(tab, rozmiar);
//----------------------------------ZEGAR----------------------------------------------
	start = clock();
	InsertSort(tab, rozmiar);
	stop = clock();
	time = (double)(stop - start) / CLOCKS_PER_SEC;
	sum_after = sum(tab, rozmiar);
	cout << "Podaj nazwe pliku wyjsciowego: ";
	cin >> exit;
	writeToFile(exit,tab, rozmiar);
	delete[] tab;
//----------------------------------KONCOWE WYPISANIE----------------------------------
	cout << "Suma liczb przed sortowaniem to: " << sum_before << endl;
	cout << "Suma liczb po sortowaniu to: " << sum_after << endl;
	cout << "Czas sortowania to: " << time << endl;
	system("pause");
	return 0;
}
