#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

struct osoba {
	string imie;
	string nazwisko;
	int rok;
	osoba *nastepna;
	osoba();
};
osoba::osoba() {
	nastepna = 0;
}
struct lista {
	osoba *pierwsza;
	void dodaj(string imie, string nazwisko, int wiek);
	void wyswietl();
	int ileElementow();
	osoba elementK(int k);
	void usunElementK(int k);
	lista();
};
lista::lista() {
	pierwsza = 0;
}
bool isAlphabetical(string addImie, string addNazwisko, string imie, string nazwisko) {
	string addRecord = addNazwisko + addImie;
	string compareRecord = nazwisko + imie;
	int len = addRecord.length() < compareRecord.length() ? addRecord.length() : compareRecord.length();
	for (int i = 0; i < len; i++)
	{
		if (addRecord[i] > compareRecord[i]) return false;
		if (addRecord[i] < compareRecord[i]) return true;
	}
	return addRecord.length() > compareRecord.length() ? true : false;
	return true;
}
int lista::ileElementow() {
	int ile = 1;
	if (pierwsza == 0) {
		return 0;
	}
	else {
		osoba *temp = pierwsza;
		while (temp->nastepna) {
			temp = temp->nastepna;
			ile++;
		}
		return ile;
	}
}
osoba lista::elementK(int k) {
	osoba *temp = pierwsza;
	for (int i = 1; i < k; i++) {
		temp = temp->nastepna;
	}
	return *temp;
}
void lista::usunElementK(int k) {
	if (k == 1) {
		osoba *temp = pierwsza;
		pierwsza = temp->nastepna;
	}
	if (k >= 2) {
		int j = 1;
		osoba *temp = pierwsza;
		while (temp) {
			if ((j + 1) == k) break;
			temp = temp->nastepna;
			j++;
		}
		if (temp->nastepna->nastepna == 0)
			temp->nastepna = 0;
		else
			temp->nastepna = temp->nastepna->nastepna;
	}
}
void lista::dodaj(string imie, string nazwisko, int rok) {
	osoba *nowa = new osoba;
	bool koniec = 0;
	nowa->imie = imie;
	nowa->nazwisko = nazwisko;
	nowa->rok = rok;
	if (pierwsza == 0) pierwsza = nowa;
	else {
		osoba *temp = pierwsza;
		if (temp->nastepna == 0) {
			if (isAlphabetical(imie, nazwisko, temp->imie, temp->nazwisko)) {
				nowa->nastepna = pierwsza;
				pierwsza = nowa;
			}
			else {
				pierwsza->nastepna = nowa;
				nowa->nastepna = 0;
			}
		}
		else {
			while (!isAlphabetical(imie, nazwisko, temp->nastepna->imie, temp->nastepna->nazwisko)) {
				temp = temp->nastepna;
				if (temp->nastepna == 0) {
					koniec = 1;
					break;
				}
			}
			if (koniec) {
				temp->nastepna = nowa;
				nowa->nastepna = 0;
			}
			else {
				nowa->nastepna = temp->nastepna;
				temp->nastepna = nowa;
			}
		}

	}
}
void tabSpace(int ile) {
	for (int i = 0; i < ile; i++)
	{
		cout << " ";
	}
}
void lista::wyswietl() {
	int ile = 20;
	if (pierwsza == 0) cout << "Lista jest pusta" << endl;
	else {
		osoba *temp = pierwsza;
		while (temp) {
			cout << "nazwisko: " << temp->nazwisko;
			tabSpace(ile - temp->nazwisko.length());
			cout << " | imie: " << temp->imie;
			tabSpace(ile - temp->imie.length());
			cout << " | rok: " << temp->rok << endl;
			temp = temp->nastepna;
		}
	}
}



int main()
{
	lista *baza = new lista;
	baza->dodaj("Herbert", "Dziedzic", 1993);
	baza->dodaj("Jakub", "Rutkowski", 1993);
	baza->dodaj("Adam", "Kozlowski", 1993);
	baza->dodaj("Bartek", "Swiatek", 1993);
	baza->dodaj("Oskar", "Krajewski", 1993);
	baza->dodaj("Aleksandra", "Gajda", 1993);
	baza->dodaj("Aniela", "Jankowska", 1993);
	baza->dodaj("Klaudia", "Sokolowska", 1993);
	baza->wyswietl();
	cout << "Ilosc elementow w bazie wynosi: " << baza->ileElementow() << endl;
	cout << endl << "---------------------------------------------------------------------------------" << endl << endl;
	cout << "Element 7 w bazie to: " << baza->elementK(7).nazwisko << " | " << baza->elementK(7).imie << " | " << baza->elementK(7).rok << endl << endl;
	baza->usunElementK(7);
	cout << "Baza po usunieciu 7 elementu: " << endl;
	baza->wyswietl();
	cout << "Ilosc elementow w bazie wynosi: " << baza->ileElementow() << endl;
	delete (baza);
	return 0;
}

