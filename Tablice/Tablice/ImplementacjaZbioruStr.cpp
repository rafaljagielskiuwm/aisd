#include "pch.h"
#include "SumaPowtarzanie.h"
#include "OdwrocenieEratostenes.h"
#include <iostream>
#include <conio.h>
#include <string>
using namespace std;
void menu2() {
	system("cls");
	cout << "Wybierz nr zadania:\n";
	cout << "0.EXIT\n";
	cout << "1.Dodaj napis\n";
	cout << "2.Usun napis\n";
	cout << "3.Wyswietl\n";
	cout << "4.Wyczysc tablice\n";
	cout << "5.Sprawdz czy tablica jest pelna\n";
}
bool czyPelny(string *tab, int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] == "") return false;
	}
	return true;
}
void czysc(string *tab, int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		tab[i] = "";
	}
}
bool czyPowt(string *tab, int rozmiar, string napis) {
	for (int i = 0; i < rozmiar; i++)
	{
		if (napis == tab[i])return true;
	}
	return false;
}
void usun(string *tab, int rozmiar) {
	string napis;
	int licz = 0;
	cout << "Podaj napis ktory chcesz usunac z tablicy: ";
	cin >> napis;
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] == napis) {
			licz++;
			tab[i] ="";
			cout << "Usunieto napis: " << napis << endl;
		}
	}
	if (licz == 0) cout << "W tablicy nie ma napisu: " << napis << endl;
}
void dodaj(string *tab, int rozmiar) {
	string napis;
	cout << "Jaki napis chcesz dodac: ";
	cin >> napis;
	if (czyPowt(tab, rozmiar, napis)) cout << "Ten napis juz jest w tablicy" << endl;
	else if (czyPelny(tab,rozmiar)) cout << "Tablica jest juz pelna" << endl;
	else {
		for (int i = 0; i < rozmiar; i++)
		{
			if (tab[i] == "") {
				tab[i] = napis;
				cout << "Dodano napis do tablicy" << endl;
				break;
			}
		}
		
	}
}
void wyswietl(string *tab, int rozmiar) {
	int licz = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] != "") {
			licz++;
			cout << tab[i] << ", ";
		}
	}
	if (licz == 0) cout << "Tablica jest pusta";
	cout << endl;
}

void zad36() {
	_getch();
	int n;
	string *tab;
	cout << "Podaj rozmiar tablicy napisow: ";
	cin >> n;
	tab = new string[n];
	czysc(tab, n);
	char wybor2 = 's';
	while (wybor2 != '0')
	{
		menu2();
		wybor2 = _getch();
		switch (wybor2)
		{
		case '0':
			return;
			break;
		case '1':
			dodaj(tab,n);
			break;
		case '2':
			usun(tab,n);
			break;
		case '3':
			wyswietl(tab,n);
			break;
		case '4':
			czysc(tab, n);
			break;
		case '5':
			if (czyPelny(tab, n)) cout << "Tablica jest pelna" << endl;
			else cout << "Sa jeszcze wolne miejsca w tablicy" << endl;
			break;
		default:
			cout << "Wybrano zla opcje\n";
			break;
		}
		_getch();
		system("pause");
		system("cls");
	}
	delete[] tab;
}