#include "pch.h"
#include "SumaPowtarzanie.h"
#include "OdwrocenieEratostenes.h"
#include <iostream>
#include <conio.h>
using namespace std;
void menu1() {
	system("cls");
	cout << "Wybierz nr zadania:\n";
	cout << "0.EXIT\n";
	cout << "1.Dodaj liczbe\n";
	cout << "2.Usun liczbe\n";
	cout << "3.Wyswietl\n";
}
void dodaj(int *tab, int rozmiar = 100) {
	int liczba;
	cout << "Jaka liczbe chcesz dodac: ";
	cin >> liczba;
	if (czyPowt(tab, rozmiar, liczba)) cout << "Ta liczba juz jest w tablicy" << endl;
	else {
		for (int i = 0; i < rozmiar; i++)
		{
			if (tab[i] == NULL) {
				tab[i] = liczba;
				break;
			}
		}
		cout << "Dodano liczbe do tablicy" << endl;
	}
}
void usun(int *tab, int rozmiar = 100) {
	int liczba = 0;
	int licz = 0;
	cout << "Podaj liczbe ktora chcesz usunac z tablicy: ";
	cin >> liczba;
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] == liczba) {
			licz++;
			tab[i] = NULL;
			cout << "Usunieto liczbe: " << liczba << endl;
		}
	}
	if (licz == 0) cout << "W tablicy nie ma liczby: " << liczba << endl;
}
void wyswietl(int *tab, int rozmiar = 100) {
	int licz = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] != NULL) {
			licz++;
			cout << tab[i] << ", ";
		}
	}
	if (licz == 0) cout << "Tablica jest pusta";
	cout << endl;
}

void zad35() {
	_getch();
	int tab[100];
	for (int i = 0; i < 100; i++)
	{
		tab[i] = NULL;
	}
	char wybor1 = 's';
	while (wybor1 != '0')
	{
		menu1();
		wybor1 = _getch();
		switch (wybor1)
		{
		case '0':
			return;
			break;
		case '1':
			dodaj(tab);
			break;
		case '2':
			usun(tab);
			break;
		case '3':
			wyswietl(tab);
			break;
		default:
			cout << "Wybrano zla opcje\n";
			break;
		}
		_getch();
		system("pause");
		system("cls");
	}
}