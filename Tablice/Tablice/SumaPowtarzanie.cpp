#include "pch.h"
#include <iostream>
#include <conio.h>
using namespace std;
bool czyPowt(int *tab, int rozmiar, int liczba) {
	for (int i = 0; i < rozmiar; i++)
	{
		if (liczba == tab[i])return true;
	}
	return false;
}
void zaznaczPowt(int liczba, int *tab, int rozmiar, bool *spr) {
	int licz = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		if (liczba == tab[i]) {
			licz++;
			if ((licz == 2) && (!spr[i])) spr[i] = true;
			else if (!spr[i]) spr[i] = false;
		}
	}
}
void zad31() {
	int sumaU = 0;
	int sumaD = 0;
	int *tab;
	int rozmiar;
	cout << "Ile liczb chcesz wpisac: ";
	cin >> rozmiar;
	tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		cout << "Podaj liczbe nr " << i + 1 << " :";
		cin >> tab[i];
		if (tab[i] > 0) sumaD += tab[i];
		else sumaU+= tab[i];
	}
	cout << "Suma ujemnych wynosi: " << sumaU << endl;
	cout << "Suma dodatnich wynosi: " << sumaD << endl;
	delete[] tab;
}
void zad32() {
	int *tab;
	bool *spr;
	int rozmiar;
	cout << "Ile liczb chcesz wpisac: ";
	cin >> rozmiar;
	tab = new int[rozmiar];
	spr = new bool[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		cout << "Podaj liczbe nr " << i + 1 << " :";
		cin >> tab[i];
		spr[i] = false;
	}
	for (int i = 0; i < rozmiar; i++)
	{
		zaznaczPowt(tab[i], tab, rozmiar, spr);
	}
	cout << "Liczby ktore sie powtarzaja to: ";
	for (int i = 0; i < rozmiar; i++)
	{
		if (spr[i]) cout << tab[i] << ", ";
	}
	cout << endl;
	delete[] tab;
	delete[] spr;
}
