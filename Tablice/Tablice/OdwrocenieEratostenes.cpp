#include "pch.h"
#include <iostream>
#include <conio.h>
using namespace std;
void odwroc(int *tab, int rozmiar, int i = 0) {
	int pom;
	if (i >= (rozmiar / 2)) return;
	else {
		pom = tab[i];
		tab[i] = tab[rozmiar - i - 1];
		tab[rozmiar - i - 1] = pom;
		odwroc(tab, rozmiar, ++i);
	}
}
void erato(int *tab, int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		if (tab[i] != 0) {
			for (int j = 0; j < rozmiar; j++)
			{
				if ((i != j) && (tab[j] % tab[i] == 0)) tab[j] = 0;
			}
		}
	}
}
void zad33() {
	int tab[10] = { 0,1,2,3,4,5,6,7,8,9 };
	for (int i = 0; i < 10; i++)
	{
		cout << tab[i] << ", ";
	}
	cout << endl;
	odwroc(tab, 10);
	for (int i = 0; i < 10; i++)
	{
		cout << tab[i] << ", ";
	}
	cout << endl;
}
void zad34() {
	int n;
	int *tab;
	cout << "Podaj koniec przedzialu: ";
	cin >> n;
	tab = new int[n - 1];
	for (int i = 0; i < n - 1; i++)
	{
		tab[i] = i + 2;
	}
	erato(tab, n - 1);
	cout << "Liczby pierwsze z przedzialu 2 do " << n << " to: ";
	for (int i = 0; i < n - 1; i++)
	{
		if (tab[i] != 0) cout << tab[i] << ", ";
	}
	cout << endl;
	delete[] tab;
}