#include "pch.h"
#include "Menu.h"
#include "SumaPowtarzanie.h"
#include "OdwrocenieEratostenes.h"
#include "ImplementacjaZbioruInt.h"
#include "ImplementacjaZbioruStr.h"
#include <iostream>
#include <conio.h>
using namespace std;
void menu() {
	cout << "Wybierz nr zadania:\n";
	cout << "0.EXIT\n";
	cout << "1.Suma ujemnych i dodatnich\n";
	cout << "2.Liczby ktore sie powtarzaja\n";
	cout << "3.Rekurencyjne odwrocenie tablicy\n";
	cout << "4.Znajdz wszystkie liczby pierwsze z przedzialu 2 do n\n";
	cout << "5.Dzialania na statycznej 100 elementowej tablicy\n";
	cout << "6.Dzialania na dynamicznej tablicy napisow\n";
}
void start() {
	char wybor;
	while (true) {
		menu();
		wybor = _getch();
		switch (wybor)
		{
		case '0':
			exit(0);
			break;
		case '1':
			zad31();
			break;
		case '2':
			zad32();
			break;
		case '3':
			zad33();
			break;
		case '4':
			zad34();
			break;
		case '5':
			zad35();
			break;
		case '6':
			zad36();
			break;
		default:
			cout << "Wybrano zla opcje\n";
			break;
		}
		_getch();
		system("pause");
		system("cls");
	}
}